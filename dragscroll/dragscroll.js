/*
 For reseting dragscrolling run
 dragscroll.reset();
 */
var initDragscroll = function() {

    var _window = window;
    var _document = document;
    var mousemove = 'mousemove';
    var mouseup = 'mouseup';
    var mousedown = 'mousedown';
    var EventListener = 'EventListener';
    var addEventListener = 'add' + EventListener;
    var removeEventListener = 'remove' + EventListener;

    var dragged = [];
    var reset = function (i, el) {
        for (i = 0; i < dragged.length;) {
            el = dragged[i++];
            el = el.container || el;
            el[removeEventListener](mousedown, el.md, 0);
            _window[removeEventListener](mouseup, el.mu, 0);
            _window[removeEventListener](mousemove, el.mm, 0);
        }

        // cloning into array since HTMLCollection is updated dynamically
        dragged = [].slice.call(_document.getElementsByClassName('dragscroll'));
        for (i = 0; i < dragged.length;) {
            (function (el, lastClientX, lastClientY, pushed, scroller, cont, moved) {
                (cont = el.container || el)[addEventListener](
                    mousedown,
                    cont.md = function (e) {
                        if (!el.hasAttribute('nochilddrag') ||
                            _document.elementFromPoint(
                                e.pageX, e.pageY
                            ) == cont
                        ) {
                            pushed = 1;
                            moved = 0;
                            lastClientX = e.clientX;
                            lastClientY = e.clientY;

                            e.preventDefault();
                        }
                    }, 0
                );

                _window[addEventListener](
                    mouseup, cont.mu = function (e) {
                        if(pushed){
                            pushed = 0;

                            if(moved>=10 && !$(e.target).hasClass('dragscroll')){
                                var current_el = $(e.target).closest('.dragscroll');
                                if(current_el.length>0 && current_el.is(el)){
                                    window.block_click = true;
                                    e.stopPropagation();
                                    return false;
                                }
                            }
                        }
                    }, 0
                );

                _window[addEventListener](
                    mousemove,
                    cont.mm = function (e) {
                        if (pushed) {
                            (scroller = el.scroller || el).scrollLeft -=
                                (-lastClientX + (lastClientX = e.clientX));
                            scroller.scrollTop -=
                                (-lastClientY + (lastClientY = e.clientY));
                            if(moved<10){
                                moved++;
                            }
                        }
                    }, 0
                );
            })(dragged[i++]);
        }
    };


    if (_document.readyState == 'complete') {
        reset();
    } else {
        _window[addEventListener]('load', reset, 0);
    }

    return {reset: reset};
};