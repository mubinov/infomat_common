// путь к папке /pdf
if(!window.pdf_path){
    if(document.currentScript){
        var src = document.currentScript.src;
        window.pdf_path = src.split('/').slice(0, -1).join('/') + '/';
    }else{
        window.pdf_path = '../pdf/';
    }
}

$(document).on('click', 'a[href$=".pdf"]', function(){

    // перехват кликов на ссылки с pdf
    var pdf_url = $(this).attr('href');

    // путь к файлу
    if(pdf_url.slice(0,1)!='/'){
        // путь не абсолютный
        var pathname = document.location.pathname;
        if(pathname.slice(0,1)!='/'){
            pathname = '/'+pathname;
        }
        pathname = pathname.split('/').slice(0, -1).join('/') + '/';
        pdf_url = document.location.protocol + '//' + document.location.host + pathname + pdf_url;
    }

    var url = window.pdf_path + 'web/viewer.html?file=' + pdf_url;
    var styles = 'position: absolute;top:0;left:0;width:1080px;height:1760px;border:0;';

    var pdf_iframe = '<iframe src="' + url+ '" id="pdf_iframe" style="'+styles+'"></iframe>';

    if($('#pdf_iframe').length!=0){
        $('#pdf_iframe').remove();
    }

    $('body').append(pdf_iframe);

    return false;
});