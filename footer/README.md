# Нижний блок на Инфомате

Единые стили для футера:

HTML код:

    <div class="bottom-pane">
        <span class="arrow-back">Назад</span>
        <span class="arrow-forward arrow-disabled">Вперед</span>
        <a href="http://92.53.101.102/" class="bottom-pane__btn">Главное меню</a>
    </div>
    
CSS:
    footer.css
    
Чтоб сделать кнопку Назад/Вперед неактивной нужно добавить класс `arrow-disabled`