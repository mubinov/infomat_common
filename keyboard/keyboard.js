/*
  Key module
 */
function Key(params) {
  if (Object.prototype.toString.call(params) == "[object Arguments]") {
    this.keyboard = params[0];
  } else {
    this.keyboard = params;
  }

  this.$key = $("<li/>");
  this.current_value = null;
}

Key.prototype.render = function() {
  if (this.id) {
    this.$key.attr("id", this.id);
  }

  if(this.title){
    this.default_value = this.title;
  }

  if(this.new_line){
    this.$key.addClass('new_line');
  }

  if(this.classes){
    this.$key.addClass(this.classes);
  }

  return this.$key;
};

Key.prototype.setCurrentValue = function() {
  if (this.keyboard.upperRegister()) {
    this.current_value = this.preferences.u ? this.preferences.u : this.default_value;
  } else {
    this.current_value = this.preferences.d ? this.preferences.d : this.default_value;
  }

  if(this.id==null && this.current_value!=null){
    this.id = 'mlkeyboard-'+this.current_value.charCodeAt();
    this.$key.attr("id", this.id);
  }

  if(this.is_html){
    this.$key.html(this.current_value);
  }else{
    this.$key.text(this.current_value);
  }
};

Key.prototype.setCurrentAction = function() {
  var _this = this;

  this.$key.unbind("mousedown.mlkeyboard");
  this.$key.bind("mousedown.mlkeyboard", function(){
    _this.keyboard.keep_focus = true;

    if (typeof(_this.preferences.onMousedown) === "function") {
      _this.preferences.onMousedown(_this);
    } else {
      _this.defaultClickAction();
    }
  });
};

Key.prototype.defaultClickAction = function() {
  this.keyboard.destroyModifications();

  if (this.is_modificator) {
    this.keyboard.deleteChar();
    this.keyboard.printChar(this.current_value);
  } else {
    this.keyboard.printChar(this.current_value);
  }

  if (this.preferences.m && Object.prototype.toString.call(this.preferences.m) === '[object Array]') {
    this.showModifications();
  }

  if (this.keyboard.active_shift) this.keyboard.toggleShift(false);
};

Key.prototype.showModifications = function() {
  var _this = this;

  this.keyboard.modifications = [];

  $.each(this.preferences.m, function(i, modification) {
    var key = new Key(_this.keyboard);
    key.is_modificator = true;
    key.preferences = modification;
    _this.keyboard.modifications.push(key);
  });

  this.keyboard.showModifications(this);
};

Key.prototype.toggleActiveState = function() {
  if (this.isActive() ) {
    this.$key.addClass('red');
  } else {
    if(this.$key.attr('id') != 'mlkeyboard-return'){
      this.$key.removeClass('red');
    }
  }
};

Key.prototype.isActive = function() {
  return false;
};


/*
----------------------------------------------------------------

 Custom Buttons module

 */
var backspace_svg = '<svg height="18px" version="1.1" viewBox="0 0 24 18" width="24px"><g fill="none" fill-rule="evenodd" stroke="none" stroke-width="1"><g fill="#ffffff" id="Core" transform="translate(-84.000000, -45.000000)"><g transform="translate(84.000000, 45.000000)"><path d="M22,0 L7,0 C6.3,0 5.8,0.3 5.4,0.9 L0,9 L5.4,17.1 C5.8,17.6 6.3,18 7,18 L22,18 C23.1,18 24,17.1 24,16 L24,2 C24,0.9 23.1,0 22,0 L22,0 Z M19,12.6 L17.6,14 L14,10.4 L10.4,14 L9,12.6 L12.6,9 L9,5.4 L10.4,4 L14,7.6 L17.6,4 L19,5.4 L15.4,9 L19,12.6 L19,12.6 Z"/></g></g></g></svg>';

// DELETE
function KeyDelete() {
  Key.call(this, arguments);

  this.id = "mlkeyboard-backspace";
  this.default_value = backspace_svg;
  this.is_html = true;
}

KeyDelete.prototype = new Key();
KeyDelete.prototype.constructor = KeyDelete;

KeyDelete.prototype.defaultClickAction = function() {
  this.keyboard.deleteChar();
};

//function KeyTab() {
//  Key.call(this, arguments);
//
//  this.id = "mlkeyboard-tab";
//  this.default_value = 'tab';
//}
//
//KeyTab.prototype = new Key();
//KeyTab.prototype.constructor = KeyTab;
//
//KeyTab.prototype.defaultClickAction = function() {
//  this.keyboard.$current_input.next(":input").focus();
//};
function KeyCapsLock() {
 Key.call(this, arguments);

 this.id = "mlkeyboard-capslock";
 this.default_value = 'CapsLk';
}

KeyCapsLock.prototype = new Key();
KeyCapsLock.prototype.constructor = KeyCapsLock;

KeyCapsLock.prototype.isActive = function() {
 return this.keyboard.active_caps;
};

KeyCapsLock.prototype.defaultClickAction = function() {
 this.keyboard.toggleCaps();
};

// RETURN
var return_svg = '<?xml version="1.0" ?><svg height="16px" version="1.1" viewBox="0 0 16 16" width="16px" xmlns="http://www.w3.org/2000/svg" xmlns:sketch="http://www.bohemiancoding.com/sketch/ns" xmlns:xlink="http://www.w3.org/1999/xlink"><title/><desc/><defs/><g fill="none" fill-rule="evenodd" id="Page-1" stroke="none" stroke-width="1"><g fill="#ffffff" id="Core" transform="translate(-4.000000, -46.000000)"><g id="arrow-forward" transform="translate(4.000000, 46.000000)"><path d="M8,0 L6.6,1.4 L12.2,7 L0,7 L0,9 L12.2,9 L6.6,14.6 L8,16 L16,8 L8,0 Z" id="Shape"/></g></g></g></svg>';

function KeyReturn() {
  Key.call(this, arguments);

  this.id = "mlkeyboard-return";

  if(window.is_infomat_app){
    this.default_value = 'Найти';
  }else{
    this.is_html = true;
    this.default_value = return_svg;
  }
}

KeyReturn.prototype = new Key();
KeyReturn.prototype.constructor = KeyReturn;

KeyReturn.prototype.defaultClickAction = function() {
  var form = this.keyboard.$current_input.closest('form');
  if(form){
    form.submit();
  }
  
  this.keyboard.hideKeyboard();
};

//function KeyShift() {
//  Key.call(this, arguments);
//
//  this.id = "mlkeyboard-"+arguments[1]+"-shift";
//  this.default_value = 'shift';
//}

//KeyShift.prototype = new Key();
//KeyShift.prototype.constructor = KeyShift;
//
//KeyShift.prototype.isActive = function() {
//  return this.keyboard.active_shift;
//};
//
//KeyShift.prototype.defaultClickAction = function() {
//  this.keyboard.toggleShift();
//};

// SPACE

function KeySpace() {
  Key.call(this, arguments);

  this.id = "mlkeyboard-space";
  this.default_value = ' ';
}

KeySpace.prototype = new Key();
KeySpace.prototype.constructor = KeySpace;

KeySpace.prototype.defaultClickAction = function() {
  this.keyboard.destroyModifications();
  this.keyboard.printChar(' ');
};


// CHANGE LAYOUT
function ChangeLayout() {
  Key.call(this, arguments);

  this.id = "mlkeyboard-change-layout";
  this.default_value = 'EN';

  var layout = arguments[1] ? arguments[1].layout : null;
  if(!layout){
    layout = 'en_US';
  }

  this.layout = layout;
}

ChangeLayout.prototype = new Key();
ChangeLayout.prototype.constructor = ChangeLayout;

ChangeLayout.prototype.defaultClickAction = function() {
  window.layout_changing = true;
  this.keyboard.changeLayout(this.layout);
  // this.keyboard.$current_input.mlKeyboard({layout: this.layout});
  //
  var self = this;
  setTimeout(function(){
    self.keyboard.$current_input.focus();
  }, 100);
};

/*
 ----------------------------------------------------------------

 Keyboard module

 */

function Keyboard(selector, options){
  this.defaults = {
    layout: 'ru_RU',
    active_shift: false,
    active_caps: true,
    show_caps_lock: false,
    is_hidden: true,
    open_speed: 10,
    close_speed: 10,
    show_on_focus: true,
    hide_on_blur: true,
    trigger: undefined,
    enabled: true
  };

  this.global_options = $.extend({}, this.defaults, options);
  this.options = $.extend({}, {}, this.global_options);

  this.keys = [];

  if($('#mlkeyboard').length){
    $('#mlkeyboard').remove();
  }
  
  this.$keyboard = $("<div/>").attr("id", "mlkeyboard");
  this.$modifications_holder = $("<ul/>").addClass('mlkeyboard-modifications');
  this.$current_input = $(selector);
  this.$keyboard.toggleClass("caps-enabled", this.options.show_caps_lock);

  window.mlkeyboard = this;
}

Keyboard.prototype.init = function() {
  this.$keyboard.append(this.renderKeys());
  this.$keyboard.append(this.$modifications_holder);
  $("body").append(this.$keyboard);

  if(window.layout_changing){
    $("body").addClass('keyboard');
  }else{
    this.$keyboard.hide();
  }
  window.layout_changing = false;

  var self = this;
  this.$keyboard.mousedown(function(){
    self.keep_focus = true;
  });

  if(window.keyboard_mousedown_catcher==null){

    $(document).on('mousedown',function(e){
      if($('#mlkeyboard').is(":visible")){
        var target = $(e.target);

        if(!(
              target.attr('id') == 'mlkeyboard' ||
              target.attr('id') == 'mlkeyboard-change-layout' ||
              target.closest('#mlkeyboard').length ||
              target.hasClass('keyboard-ctrl')
            )
        ) {
          window.mlkeyboard.hideKeyboard();
        }
      }
    });
    window.keyboard_mousedown_catcher = true;
  }

  this.active_shift = this.options.active_shift;
  this.active_caps = this.options.active_caps;
  this.setUpKeys();
};

Keyboard.prototype.changeLayout = function(layout) {
  this.options.layout = layout;
  this.keys = [];

  this.$keyboard.find('ul:first-child').remove();
  this.$keyboard.prepend(this.renderKeys());

  this.setUpKeys();
};

Keyboard.prototype.setUpKeys = function() {
  var _this = this;

  $.each(this.keys, function(i, key){

    key.preferences = mlKeyboard.layouts[_this.options.layout][i];
    key.setCurrentValue();
    key.setCurrentAction();
    key.toggleActiveState();
  });
};

Keyboard.prototype.renderKeys = function() {
  var $keys_holder = $("<ul/>"),
      max_iter = mlKeyboard.layouts[this.options.layout].length - 1;

  for (var i = 0; i <= max_iter; i++) {
    var key,
        layout_item = mlKeyboard.layouts[this.options.layout][i];

    if(layout_item.action != null){

      var params = layout_item.params;

      if(params==null){
        params = null;
      }

      key = new window[layout_item.action](this, params);
    }else{
      key = new Key(this);
    }

    key.new_line = layout_item.new_line;
    key.title = layout_item.title;
    key.classes = layout_item.classes;
    this.keys.push(key);
    $keys_holder.append(key.render());
  }

  return $keys_holder;
};

Keyboard.prototype.setUpFor = function($input) {
  var _this = this;

  $input.addClass('keyboard-ctrl');

  if (this.options.show_on_focus) {
    $input
        .unbind('focus')
        .bind('focus', function() { _this.showKeyboard($input); });
  }

  // if (this.options.hide_on_blur) {
  //   setTimeout(function(){
  //     $input
  //         .unbind('blur')
  //         .bind('blur', function() {
  //           // Input focus changes each time when user click on keyboard key
  //           // To prevent momentary keyboard collapse input state verifies with timers help
  //           // Any key click action set current inputs keep_focus variable to true
  //           clearTimeout(_this.blur_timeout);
  //
  //           _this.blur_timeout = setTimeout(function(){
  //             if (!_this.keep_focus) {
  //               _this.hideKeyboard();
  //             } else {
  //               _this.keep_focus = false;
  //             }
  //           }, 200);
  //         });
  //   }, 0);
  // }

  if (this.options.trigger) {
    var $trigger = $(this.options.trigger);
    $trigger.bind('click', function(e) {
      e.preventDefault();

      if (_this.isVisible) { _this.hideKeyboard(); }
      else {
        _this.showKeyboard($input);
        $input.focus();
      }
    });
  }
};

Keyboard.prototype.showKeyboard = function($input) {
  var input_changed = !this.$current_input || $input[0] !== this.$current_input[0];

  if (!this.keep_focus || input_changed) {
    if (input_changed) this.keep_focus = false;

    this.$current_input = $input;
    this.options = $.extend({}, this.global_options, this.inputLocalOptions());

    if (!this.options.enabled) {
      this.keep_focus = false;
      return;
    }

    if (this.$current_input.val() !== '') {
      this.options.active_shift = false;
    }

    this.$keyboard.toggleClass('mode-digital', $input.data('keyboard-mode') == 'digital');

    //this.setUpKeys();
    $("body").addClass('keyboard');

    if (this.options.is_hidden) {
      this.isVisible = true;
      this.$keyboard.slideDown();
    }
  }
};

Keyboard.prototype.hideKeyboard = function() {
  if (this.options.is_hidden) {
    this.isVisible = false;
    this.$keyboard.slideUp();
    this.$current_input = {};
    $("body").removeClass('keyboard');
  }
};

Keyboard.prototype.inputLocalOptions = function() {
  var options = {};
  for (var key in this.defaults) {
    var input_option = this.$current_input.attr("data-mlkeyboard-"+key);
    if (input_option == "false") {
      input_option = false;
    } else if (input_option == "true") {
      input_option = true;
    }
    if (typeof input_option !== 'undefined') { options[key] = input_option; }
  }

  return options;
};

Keyboard.prototype.printChar = function(char) {
  if(this.$current_input == null || this.$current_input.length == 0){
    console.error('Input field not selected');
    return false;
  }

  var selStart = this.$current_input[0].selectionStart;
  var selEnd = this.$current_input[0].selectionEnd;
  var textAreaStr = this.$current_input.val();
  var value = textAreaStr.substring(0, selStart) + char + textAreaStr.substring(selEnd);

  this.$current_input.val(value).focus();
  this.$current_input[0].selectionStart = selStart+1;
  this.$current_input[0].selectionEnd = selStart+1;

  self = this;
  setTimeout(function(){
    self.$current_input.focus().mouseup().keypress().keyup().keydown();
  }, 100);
};

Keyboard.prototype.deleteChar = function() {
  var selStart = this.$current_input[0].selectionStart;
  var selEnd = this.$current_input[0].selectionEnd;

  var textAreaStr = this.$current_input.val();
  var after = textAreaStr.substring(0, selStart-1);
  var value = after + textAreaStr.substring(selEnd);
  this.$current_input.val(value).focus();
  this.$current_input[0].selectionStart = selStart-1, this.$current_input[0].selectionEnd = selStart-1;

  self = this;
  setTimeout(function(){
    self.$current_input.focus().mouseup().keypress().keyup().keydown();
  }, 100);
};

Keyboard.prototype.showModifications = function(caller) {
  var _this = this,
      holder_padding = parseInt(_this.$modifications_holder.css('padding'), 10),
      top, left, width;

  $.each(this.modifications, function(i, key){
    _this.$modifications_holder.append(key.render());

    key.setCurrentValue();
    key.setCurrentAction();
  });

  // TODO: Remove magic numbers
  width = (caller.$key.width() * _this.modifications.length) + (_this.modifications.length * 6);
  top = caller.$key.position().top - holder_padding;
  left = caller.$key.position().left - _this.modifications.length * caller.$key.width()/2;

  this.$modifications_holder.one('mouseleave', function(){
    _this.destroyModifications();
  });

  this.$modifications_holder.css({
    width: width,
    top: top,
    left: left
  }).show();
};

Keyboard.prototype.destroyModifications = function() {
  this.$modifications_holder.empty().hide();
};

Keyboard.prototype.upperRegister = function() {
  return ((this.active_shift && !this.active_caps) || (!this.active_shift && this.active_caps));
};

Keyboard.prototype.toggleShift = function(state) {
  this.active_shift = state ? state : !this.active_shift;
  this.changeKeysState();
};

Keyboard.prototype.toggleCaps = function(state) {
  this.active_caps = state ? state : !this.active_caps;
  this.changeKeysState();
};

Keyboard.prototype.changeKeysState = function() {
  $.each(this.keys, function(_, key){
    key.setCurrentValue();
    key.toggleActiveState();
  });
};


$.fn.mlKeyboard = function(options) {
  var keyboard = new Keyboard(this.selector, options);
  keyboard.init();

  this.each(function(){
    keyboard.setUpFor($(this));
  });
};

/*
 ----------------------------------------------------------------

 Layout list

 */

var mlKeyboard = mlKeyboard || {layouts: {}};

mlKeyboard.layouts.en_US = [

  {d: '!',  u: '1', classes: 'number'},
  {d: '\'', u: '2', classes: 'number'},
  {d: '№',  u: '3', classes: 'number'},
  {d: '%',  u: '4', classes: 'number'},
  {d: ':',  u: '5', classes: 'number'},
  {d: ',',  u: '6', classes: 'number'},
  {d: '.',  u: '7', classes: 'number'},
  {d: ';',  u: '8', classes: 'number'},
  {d: '(',  u: '9', classes: 'number'},
  {d: ')',  u: '0', classes: 'number'},

  {d: 'q',u: 'Q', classes: 'en-line-1', 'new_line': true},
  {d: 'w',u: 'W', classes: 'en-line-1'},
  {d: 'e',u: 'E', classes: 'en-line-1'},
  {d: 'r',u: 'R', classes: 'en-line-1'},
  {d: 't',u: 'T', classes: 'en-line-1'},
  {d: 'y',u: 'Y', classes: 'en-line-1'},
  {d: 'u',u: 'U', classes: 'en-line-1'},
  {d: 'i',u: 'I', classes: 'en-line-1'},
  {d: 'o',u: 'O', classes: 'en-line-1'},
  {d: 'p',u: 'P', classes: 'en-line-1'},

  {d: 'a',u: 'A', classes: 'en-line-2', 'new_line': true},
  {d: 's',u: 'S', classes: 'en-line-2'},
  {d: 'd',u: 'D', classes: 'en-line-2'},
  {d: 'f',u: 'F', classes: 'en-line-2'},
  {d: 'g',u: 'G', classes: 'en-line-2'},
  {d: 'h',u: 'H', classes: 'en-line-2'},
  {d: 'j',u: 'J', classes: 'en-line-2'},
  {d: 'k',u: 'K', classes: 'en-line-2'},
  {d: 'l',u: 'L', classes: 'en-line-2'},

  {d: 'z',u: 'Z', classes: 'en-line-3', 'new_line': true},
  {d: 'x',u: 'X', classes: 'en-line-3'},
  {d: 'c',u: 'C', classes: 'en-line-3'},
  {d: 'v',u: 'V', classes: 'en-line-3'},
  {d: 'b',u: 'B', classes: 'en-line-3'},
  {d: 'n',u: 'N', classes: 'en-line-3'},
  {d: 'm',u: 'M', classes: 'en-line-3'},
  {d: '"',u: '"', classes: 'en-line-3'},
  {d: ':',u: ':', classes: 'en-line-3'},
  {action: 'KeyDelete'},

  {action: 'KeyCapsLock'},
  {d: '"',u: '-', classes: 'line-4'},
  {d: '(',u: '(', classes: 'line-4'},
  {d: ')',u: ')', classes: 'line-4'},
  {d: '/',u: '.', classes: 'line-4'},
  {d: '@',u: '@', classes: 'line-4'},

  {title: 'RU', action: 'ChangeLayout', params: {layout: 'ru_RU'}},
  {title: 'Пробел', action: 'KeySpace'},
  {action: 'KeyReturn', classes: 'red'}
];

mlKeyboard.layouts.ru_RU = [

  {d: '!',  u: '1', classes: 'number'},
  {d: '\'', u: '2', classes: 'number'},
  {d: '№',  u: '3', classes: 'number'},
  {d: '%',  u: '4', classes: 'number'},
  {d: ':',  u: '5', classes: 'number'},
  {d: ',',  u: '6', classes: 'number'},
  {d: '.',  u: '7', classes: 'number'},
  {d: ';',  u: '8', classes: 'number'},
  {d: '(',  u: '9', classes: 'number'},
  {d: ')',  u: '0', classes: 'number'},

  {d: 'й',u: 'Й', classes: 'ru-line-1', 'new_line': true},
  {d: 'ц',u: 'Ц', classes: 'ru-line-1'},
  {d: 'у',u: 'У', classes: 'ru-line-1'},
  {d: 'к',u: 'К', classes: 'ru-line-1'},
  {d: 'е',u: 'Е', classes: 'ru-line-1'},
  {d: 'н',u: 'Н', classes: 'ru-line-1'},
  {d: 'г',u: 'Г', classes: 'ru-line-1'},
  {d: 'ш',u: 'Ш', classes: 'ru-line-1'},
  {d: 'щ',u: 'Щ', classes: 'ru-line-1'},
  {d: 'з',u: 'З', classes: 'ru-line-1'},
  {d: 'х',u: 'Х', classes: 'ru-line-1'},
  {d: 'ъ',u: 'Ъ', classes: 'ru-line-1'},

  {d: 'ф',u: 'Ф', classes: 'ru-line-2', 'new_line': true},
  {d: 'ы',u: 'Ы', classes: 'ru-line-2'},
  {d: 'в',u: 'В', classes: 'ru-line-2'},
  {d: 'а',u: 'А', classes: 'ru-line-2'},
  {d: 'п',u: 'П', classes: 'ru-line-2'},
  {d: 'р',u: 'Р', classes: 'ru-line-2'},
  {d: 'о',u: 'О', classes: 'ru-line-2'},
  {d: 'л',u: 'Л', classes: 'ru-line-2'},
  {d: 'д',u: 'Д', classes: 'ru-line-2'},
  {d: 'ж',u: 'Ж', classes: 'ru-line-2'},
  {d: 'э',u: 'Э', classes: 'ru-line-2'},

  {d: 'я',u: 'Я', classes: 'ru-line-3', 'new_line': true},
  {d: 'ч',u: 'Ч', classes: 'ru-line-3'},
  {d: 'с',u: 'С', classes: 'ru-line-3'},
  {d: 'м',u: 'М', classes: 'ru-line-3'},
  {d: 'и',u: 'И', classes: 'ru-line-3'},
  {d: 'т',u: 'Т', classes: 'ru-line-3'},
  {d: 'ь',u: 'Ь', classes: 'ru-line-3'},
  {d: 'б',u: 'Б', classes: 'ru-line-3'},
  {d: 'ю',u: 'Ю', classes: 'ru-line-3'},
  {d: 'ё',u: 'Ё', classes: 'ru-line-3'},
  {action: 'KeyDelete'},

  {action: 'KeyCapsLock'},
  {d: '"',u: '-', classes: 'line-4'},
  {d: '(',u: '(', classes: 'line-4'},
  {d: ')',u: ')', classes: 'line-4'},
  {d: '/',u: '.', classes: 'line-4'},
  {d: '@',u: ',', classes: 'line-4'},

  {title: 'EN', action: 'ChangeLayout', params: {layout: 'en_US'}},
  {title: 'Пробел', action: 'KeySpace'},
  {action: 'KeyReturn', classes: 'red'}
];


/*
 TEXT FIELD
 */

$(document).ready(function() {
  if ($('input[type=text]').length) {
    $('input[type=text]').mlKeyboard();
  }
});