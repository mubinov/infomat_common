# INFOMAT COMMON FILES

## PDF

Для отображения PDF файлов используется библиотека PDF.JS.
Для подключения нужно добавить 
(остальные файлы загружаются автоматически при необходимости):

    <script type="text/javascript" src="../pdf/pdf-wrapper.js"></script>

Принцип работы: в файле pdf-wrapper.js перехватывается событие 
клика на ссылки на pdf файлы ( ```$('a[href$=".pdf"]')``` ).
При этом загружается iframe со встроенным PDF.JS. 

## DRAGSCROLL

Файлы:

- dragscroll.js

- dragscroll.less (Сборку делать не нужно, совместим с CSS)

Для подключения dragscroll нужно добавить блоку класс *.dragscroll* . 
Инициализировать скрипт не нужно. Для перезапуска использовать *dragscroll.reset()*


## ВИРТУАЛЬНАЯ КЛАВИАТУРА

Файлы:

- keyboard.js

- keyboard.less (Сборку делать не нужно, совместим с CSS)

- bg.png

В конце keyboard.js есть вызов клавитуры:


    if($('input[type=text]').length){
      $('input[type=text]').mlKeyboard();
    }
    
Клавиатура с Caps Lock

    if($('input[type=text]').length){
      $('input[type=text]').mlKeyboard({
            show_caps_lock: true
      });
    }
Клавиатура автоматически разворачивается, когда фокус попадает в input.

## Тесты

Мануальный тест: ``` /test/index.html ```